import { lazy } from 'react';

// project import
import Loadable from 'components/Loadable';
import MainLayout from 'layout/MainLayout';

// render - dashboard
const DashboardDefault = Loadable(lazy(() => import('pages/dashboard')));

// render - sample page
const SamplePage = Loadable(lazy(() => import('pages/extra-pages/SamplePage')));

// render - utilities
const Typography = Loadable(lazy(() => import('pages/components-overview/Typography')));
const Color = Loadable(lazy(() => import('pages/components-overview/Color')));
const Shadow = Loadable(lazy(() => import('pages/components-overview/Shadow')));
const AntIcons = Loadable(lazy(() => import('pages/components-overview/AntIcons')));
const CurrentExhibitions = Loadable(lazy(() => import('pages/components-overview/CurrentExhibitions')));
const Blog01 = Loadable(lazy(() => import('pages/components-overview/Blog01')));
const Blog02 = Loadable(lazy(() => import('pages/components-overview/Blog02')));
const Blog03 = Loadable(lazy(() => import('pages/components-overview/Blog03')));
const UpcomingWorkshop = Loadable(lazy(() => import('pages/components-overview/UpcomingWorkshop')));
const SecurePayment = Loadable(lazy(() => import('pages/components-overview/SecurePayment')));
const Products = Loadable(lazy(() => import('pages/components-overview/Products')));
const AboutUs = Loadable(lazy(() => import('pages/components-overview/AboutUs')));
const Policy = Loadable(lazy(() => import('pages/components-overview/Policy')));
const Contact = Loadable(lazy(() => import('pages/components-overview/Contact')));


// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
  path: '/',
  element: <MainLayout />,
  children: [
    {
      path: '/',
      element: <DashboardDefault />
    },
    {
      path: 'color',
      element: <Color />
    },
    {
      path: 'dashboard',
      children: [
        {
          path: 'default',
          element: <DashboardDefault />
        }
      ]
    },
    {
      path: 'past-workshop',
      element: <CurrentExhibitions />
    },
    {
      path: 'sample-page',
      element: <SamplePage />
    },
    {
      path: 'shadow',
      element: <Shadow />
    },
    {
      path: 'typography',
      element: <Typography />
    },
    {
      path: 'icons/ant',
      element: <AntIcons />
    },
    {
      path: 'blog01',
      element: <Blog01 />
    },
    {
      path: 'blog02',
      element: <Blog02 />
    },
    {
      path: 'blog03',
      element: <Blog03 />
    },
    {
      path: 'upcoming-workshop',
      element: <UpcomingWorkshop />
    },
    {
      path: 'secure-payment',
      element: <SecurePayment />
    },
    {
      path: 'products',
      element: <Products />
    },
    {
      path: 'about-us',
      element: <AboutUs />
    },
    {
      path: 'policy',
      element: <Policy />
    },
    {
      path: 'contact',
      element: <Contact />
    }
  ]
};

export default MainRoutes;
