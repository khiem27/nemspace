// material-ui
import { Box } from '@mui/material';
import { useState } from 'react';
import '../../../../style/style.css';

// project import

// ==============================|| HEADER - CONTENT ||============================== //

const HeaderContent = () => {
  const mouseEnterFn = (id) => {
    setIdMenuHover(id);
  };
  const [idMenuHover, setIdMenuHover] = useState(0);

  const mouseLeaveFn = () => {
    setIdMenuHover(0);
    console.log('leave');
  };

  return (
    <Box sx={{ display: 'flex', width: '100%' }}>
      <header className="has_top scroll_header_top_area fixed scrolled_not_transparent menu_position_left page_header">
        <div className="header_inner clearfix">
          <div className="header_top_bottom_holder">
            <div className="header_bottom clearfix" style={{ backgroundColor: 'rgba(0, 0, 0, 1)' }}>
              <div className="header_inner_left">
                <div className="mobile_menu_button">
                  <span>
                    <i className="qode_icon_font_awesome fa fa-bars" aria-hidden="true"></i>
                  </span>
                </div>
                <div className="logo_wrapper" style={{ height: '100px' }}>
                  <a href="/" style={{ lineHeight: '100px' }}>
                    <h1 style={{ color: '#ffffff', fontSize: '30px', fontWeight: '700', fontFamily: 'Montserrat' }}>N É M&nbsp;&nbsp; S P A C E</h1>
                  </a>
                </div>
              </div>
              <div className="header_inner_right">
                <div className="side_menu_button_wrapper right">
                  <div className="side_menu_button" style={{ height: '100px' }}></div>
                </div>
              </div>

              <nav className="main_menu drop_down left" style={{ left: '330.875px' }}>
                <ul id="menu-top-nav-2023" className="">
                  <li
                    id="nav-menu-item-37273"
                    onMouseEnter={() => mouseEnterFn(1)}
                    onMouseLeave={() => mouseLeaveFn()}
                    className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has_sub narrow"
                  >
                    <a href="/past-workshop" className="" style={{ lineHeight: '100px' }}>
                      <i className="menu_icon blank fa" aria-hidden="true"></i>
                      <span>WORKSHOP</span>
                      <span className="plus"></span>
                    </a>
                    <div className={idMenuHover === 1 ? 'second drop_down_start' : 'second'} style={{ height: '0px' }}>
                      <div className="inner">
                        <ul>
                          <li
                            id="nav-menu-item-37245"
                            className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children sub"
                          >
                            <a href="/past-workshop" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>PAST WORKSHOP</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          
                          
                          <li id="nav-menu-item-37244" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/upcoming-workshop" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>UPCOMING WORKSHOP</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                  <li
                    onMouseEnter={() => mouseEnterFn(2)}
                    onMouseLeave={() => mouseLeaveFn()}
                    id="nav-menu-item-37272"
                    className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has_sub narrow"
                  >
                    <a href="/about-us" className="" style={{ lineHeight: '100px' }}>
                      <i className="menu_icon blank fa" aria-hidden="true"></i>
                      <span>ABOUT US</span>
                      <span className="plus"></span>
                    </a>
                    {/* <div className={idMenuHover === 2 ? 'second drop_down_start' : 'second'} style={{ height: '0px' }}>
                      <div className="inner">
                        <ul>
                          <li id="nav-menu-item-37248" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/artist-factory/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>ARTIST FACTORY</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37247" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/artist-factory-tours/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>ARTIST FACTORY TOURS</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div> */}
                  </li>
                  <li
                    onMouseEnter={() => mouseEnterFn(3)}
                    onMouseLeave={() => mouseLeaveFn()}
                    id="nav-menu-item-37274"
                    className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has_sub narrow"
                  >
                    <a href="/products" className="" style={{ lineHeight: '100px' }}>
                      <i className="menu_icon blank fa" aria-hidden="true"></i>
                      <span>PRODUCTS</span>
                      <span className="plus"></span>
                    </a>
                    {/* <div className={idMenuHover === 3 ? 'second drop_down_start' : 'second'} style={{ height: '0px' }}>
                      <div className="inner">
                        <ul>
                          <li id="nav-menu-item-37266" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/donate/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>DONATE</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37291" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/memberships/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>MEMBERSHIPS</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37250" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/merch-landing/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>STORE</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div> */}
                  </li>
                  <li
                    id="nav-menu-item-37271"
                    onMouseEnter={() => mouseEnterFn(5)}
                    onMouseLeave={() => mouseLeaveFn()}
                    className="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has_sub narrow"
                  >
                    <a href="/policy" className="" style={{ lineHeight: '100px' }}>
                      <i className="menu_icon blank fa" aria-hidden="true"></i>
                      <span>
                        POLICY
                      </span>
                      <span className="plus"></span>
                    </a>
                    {/* <div className={idMenuHover === 5 ? 'second drop_down_start' : 'second'} style={{ height: '0px' }}>
                      <div className="inner">
                        <ul>
                          <li id="nav-menu-item-37238" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/our-vision/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>VISION</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37240" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/our-center/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>SPACE</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37239" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/our-people/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>PEOPLE</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37242" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/donors/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>SUPPORTERS</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37241" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/transparency/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>TRANSPARENCY</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                          <li id="nav-menu-item-37237" className="menu-item menu-item-type-post_type menu-item-object-page">
                            <a href="/contact/" className="">
                              <i className="menu_icon blank fa" aria-hidden="true"></i>
                              <span>CONTACT</span>
                              <span className="plus"></span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div> */}
                  </li>
                  <li id="nav-menu-item-37988" className="menu-item menu-item-type-post_type menu-item-object-page narrow">
                    <a href="/contact" className="" style={{ lineHeight: '100px' }}>
                      <i className="menu_icon blank fa" aria-hidden="true"></i>
                      <span>CONTACT WITH US</span>
                      <span className="plus"></span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </header>
    </Box>
  );
};

export default HeaderContent;
