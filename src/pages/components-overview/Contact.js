import React from 'react';

Contact.propTypes = {};

function Contact() {
  return (
    <div className="content content_top_margin" style={{ minHeight: '86px' }}>
      <div className="content_inner">
        <div className="full_width" style={{ backgroundColor: '#ffffff' }}>
          <div className="full_width_inner">
            <div className="vc_row wpb_row section vc_row-fluid grid_section" style={{ paddingBottom: '30px', textAlign: 'center' }}>
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h1>Contact Us</h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <iframe
              title="Google Maps"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.20002289071!2d106.60939157570354!3d10.795986958831307!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752d9e7e71c36d%3A0x861cc3eeef0e714b!2zTmdoxKlhIHRyYW5nIELDrG5oIEjGsG5nIEjDsmE!5e0!3m2!1svi!2s!4v1701937512435!5m2!1svi!2s"
              width="100%"
              height="450"
              style={{ border: 0 }}
              allowFullScreen=""
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>

            <div className="vc_row wpb_row section vc_row-fluid grid_section" style={{ textAlign: 'left', paddingTop: '100px' }}>
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-6">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="wpcf7 js" id="wpcf7-f12-p379-o1" lang="en-US" dir="ltr">
                          <div className="screen-reader-response">
                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                            <ul></ul>
                          </div>
                          <form
                            action="/contact/#wpcf7-f12-p379-o1"
                            method="post"
                            className="wpcf7-form init cf7_custom_style_1 mailchimp-ext-0.5.70"
                            aria-label="Contact form"
                            data-status="init"
                          >
                            <p>
                              <label style={{ width: '100%' }}>
                                Your Name (required)
                                <br />
                                <span className="wpcf7-form-control-wrap" data-name="your-name">
                                  <input
                                    size="40"
                                    className="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                    aria-required="true"
                                    aria-invalid="false"
                                    value=""
                                    type="text"
                                    name="your-name"
                                  />
                                </span>
                              </label>
                            </p>
                            <p>
                              <label style={{ width: '100%' }}>
                                Your Email (required)
                                <br />
                                <span className="wpcf7-form-control-wrap" data-name="your-email">
                                  <input
                                    size="40"
                                    className="wpcf7-form-control wpcf7-email wpcf7-validates-as-required wpcf7-text wpcf7-validates-as-email"
                                    aria-required="true"
                                    aria-invalid="false"
                                    value=""
                                    type="email"
                                    name="your-email"
                                  />
                                </span>
                              </label>
                            </p>
                            <p>
                              <label style={{ width: '100%' }}>
                                Subject
                                <br />
                                <span className="wpcf7-form-control-wrap" data-name="your-subject">
                                  <input
                                    size="40"
                                    className="wpcf7-form-control wpcf7-text"
                                    aria-invalid="false"
                                    value=""
                                    type="text"
                                    name="your-subject"
                                  />
                                </span>
                              </label>
                            </p>
                            <p>
                              <label style={{ width: '100%' }}>
                                Your Message
                                <br />
                                <span className="wpcf7-form-control-wrap" data-name="your-message">
                                  <textarea
                                    cols="40"
                                    rows="10"
                                    className="wpcf7-form-control wpcf7-textarea"
                                    aria-invalid="false"
                                    name="your-message"
                                  ></textarea>
                                </span>
                              </label>
                            </p>
                            <p>
                              <span className="wpcf7-form-control-wrap" data-name="opt-in">
                                <span className="wpcf7-form-control wpcf7-checkbox">
                                  <span className="wpcf7-list-item first last">
                                    <input type="checkbox" name="opt-in[]" value="Subscribe me to your mailing list" />
                                    <span className="wpcf7-list-item-label">Subscribe me to your mailing list</span>
                                  </span>
                                </span>
                              </span>
                            </p>
                            <p>
                              <input className="wpcf7-form-control wpcf7-submit has-spinner" type="submit" value="Send" />
                              <span className="wpcf7-spinner"></span>
                            </p>
                            <div className="wpcf7-response-output" aria-hidden="true"></div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="wpb_column vc_column_container vc_col-sm-6">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h1>Say Hello!</h1>
                          </div>
                        </div>
                        <div className="vc_empty_space" style={{ height: '32px' }}>
                          <span className="vc_empty_space_inner">
                            <span className="empty_space_image"></span>
                          </span>
                        </div>

                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h4>Ném Space</h4>
                            <p>
                              <strong>Phone:</strong> 0913119034
                            </p>
                            <p>
                              <strong>Email:</strong> chao.nemproject@gmail.com
                            </p>
                            <p>
                              <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                            </p>
                            <p>
                              <strong>Hours:</strong>
                            </p>
                            <p>Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                            <p>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                            <p>&nbsp;</p>
                            <h4>
                              <em>
                                For Press and Media Inquiries:&nbsp;<a>chao.nemproject@gmail.com</a>
                              </em>
                            </h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget" >
                        <h4>Ném Space</h4>
                        <p 
                        style={{fontWeight: '400', fontSize: '16px'}}
                        >
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Hours:</strong>
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}} >Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Contact;
