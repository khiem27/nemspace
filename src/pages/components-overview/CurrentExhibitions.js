import React from 'react';

function CurrentExhibitions() {
  return (
    <>
      <div className="content_inner" style={{ padding: '100px 0 0' }}>
        <div className="full_width" style={{ backgroundColor: '#ffffff' }}>
          <div className="full_width_inner">
            <div className="vc_row wpb_row section vc_row-fluid" style={{ paddingTop: '30px', paddingBottom: '30px', textAlign: 'center' }}>
              <div className="full_section_inner clearfix">
                <div className="wpb_column vc_column_container vc_col-sm-12">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="wpb_text_column wpb_content_element">
                        <div className="wpb_wrapper">
                          <h1>Past workshop</h1>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="vc_row wpb_row section vc_row-fluid" style={{ textAlign: 'left' }}>
              <div className="full_section_inner clearfix">
                <div className="wpb_column vc_column_container vc_col-sm-12">
                  <div className="vc_column-inner">
                    <div className="wpb_wrapper">
                      <div className="blog_slider_holder clearfix">
                        <div className="blog_slider blog_slider_carousel" data-blogs_shown="3" data-auto_start="true">
                          <div
                            className="caroufredsel_wrapper"
                            style={{
                              textAlign: 'left',
                              float: 'none',
                              position: 'relative',
                              inset: 'auto',
                              zIndex: 'auto',
                              width: '1905px',
                              margin: '0px',
                              overflow: 'hidden',
                              cursor: 'move',
                              height: '354.031px'
                            }}
                          >
                            <ul
                              className="blog_slides"
                              style={{
                                textAlign: 'left',
                                float: 'none',
                                position: 'absolute',
                                inset: '0px auto auto 0px',
                                margin: '0px',
                                width: '5715px',
                                opacity: '1'
                              }}
                            >
                              <li className="item" style={{ width: '635px' }}>
                                <div className="item_holder">
                                  <div className="blog_text_holder">
                                    <a href="/blog01" className="blog_text_holder_outer">
                                      {' '}
                                    </a>
                                  </div>
                                  <div className="blog_image_holder">
                                    <span className="image">
                                      <img decoding="async" src="../../Images/1.jpg" alt="Paradboxes" width="1060" height="596" />
                                    </span>
                                  </div>
                                </div>
                              </li>
                              <li className="item" style={{ width: '635px' }}>
                                <div className="item_holder">
                                  <div className="blog_text_holder">
                                    <a href="/blog02" className="blog_text_holder_outer">
                                      {' '}
                                    </a>
                                  </div>
                                  <div className="blog_image_holder">
                                    <span className="image">
                                      <img decoding="async" src="../../Images/2.gif" alt="Paradboxes" width="1060" height="596" />
                                    </span>
                                  </div>
                                </div>
                              </li>
                              <li className="item" style={{ width: '635px' }}>
                                <div className="item_holder">
                                  <div className="blog_text_holder">
                                    <a href="/blog03" className="blog_text_holder_outer">
                                      {' '}
                                    </a>
                                  </div>
                                  <div className="blog_image_holder">
                                    <span className="image">
                                      <img decoding="async" src="../../Images/3.gif" alt="Paradboxes" width="1060" height="596" />
                                    </span>
                                  </div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget" >
                        <h4>Ném Space</h4>
                        <p 
                        style={{fontWeight: '400', fontSize: '16px'}}
                        >
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Hours:</strong>
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}} >Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default CurrentExhibitions;
