import React, { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function UpcomingWorkshop() {
  const navigate = useNavigate();
  const formRef = useRef(null);
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    phoneNumber: '',
    email: '',
    time: ''
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value
    }));
  };

  const handleConfirmClick = (event) => {
    event.preventDefault();

    // Kiểm tra xem có đủ thông tin không
    if (formData.firstName && formData.lastName && formData.phoneNumber && formData.email && formData.time) {
      console.log('Confirm clicked with data:', formData);
      navigate('/secure-payment');
    } else {
      alert('Vui lòng nhập đủ thông tin trước khi xác nhận.');
    }
  };

  return (
    <><div className="content_inner" style={{ padding: '100px 0 0' }}>
      <div className="container">
        <div className="container_inner default_template_holder">
          <div className="blog_single blog_holder" style={{ margin: '20px', padding: '20px', background: '#f8f8f8', borderRadius: '10px' }}>
            <article
              id="post-1257"
              className="post-1257 post type-post status-publish format-standard has-post-thumbnail hentry category-current-exhibitions"
            >
              <div className="post_content_holder">
                <div className="post_text">
                  <div className="post_text_inner">
                    <p>&nbsp;</p>
                    <img
                      decoding="async"
                      src="../../Images/4.gif"
                      alt="Paradboxes"
                      width="1060"
                      height="596"
                      style={{ marginBottom: '20px' }} />

                    {/* Form */}
                    <div ref={formRef} style={{ marginTop: '50px' }}>
                      <form onSubmit={handleConfirmClick}>
                        <div className="mb-3">
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="">
                                Họ và tên
                              </span>
                            </div>
                            <input
                              type="text"
                              className="form-control"
                              name="firstName"
                              value={formData.firstName}
                              onChange={handleInputChange}
                              placeholder="Họ" />
                            <input
                              type="text"
                              className="form-control"
                              name="lastName"
                              value={formData.lastName}
                              onChange={handleInputChange}
                              placeholder="Tên" />
                          </div>
                        </div>

                        <div className="mb-3">
                          <label htmlFor="phoneNumber" className="form-label">
                            Số điện thoại:
                          </label>
                          <input
                            type="tel"
                            className="form-control"
                            id="phoneNumber"
                            name="phoneNumber"
                            value={formData.phoneNumber}
                            onChange={handleInputChange} />
                        </div>

                        <div className="mb-3">
                          <label htmlFor="email" className="form-label">
                            Email:
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            id="email"
                            name="email"
                            value={formData.email}
                            onChange={handleInputChange} />
                        </div>

                        <div className="mb-3">
                          <label htmlFor="time" className="form-label">
                            Thời gian:
                          </label>

                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="time"
                              id="exampleRadios1"
                              value="9am-5pm Saturday, 16/12"
                              checked={formData.time === '9am-5pm Saturday, 16/12'}
                              onChange={handleInputChange} />
                            <label className="form-check-label" htmlFor="exampleRadios1">
                              9am-5pm Saturday, 16/12
                            </label>
                          </div>
                          <div className="form-check">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="time"
                              id="exampleRadios2"
                              value="9am-5pm Sunday, 17/12"
                              checked={formData.time === '9am-5pm Sunday, 17/12'}
                              onChange={handleInputChange} />
                            <label className="form-check-label" htmlFor="exampleRadios2">
                              9am-5pm Sunday, 17/12
                            </label>
                          </div>
                        </div>

                        <button type="submit" className="btn btn-success">
                          Confirm
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </div><footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget">
                        <h4>Ném Space</h4>
                        <p>
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p>
                          <strong>Hours:</strong>
                        </p>
                        <p>Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }} />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer></>
  );
}

export default UpcomingWorkshop;
