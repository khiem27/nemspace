import React from 'react';

Blog01.propTypes = {};

function Blog01() {
  return (
    <div className="content_inner" style={{ padding: '100px 0 0' }}>
      <div className="container">
        <div className="container_inner default_template_holder">
          <div className="blog_single blog_holder" style={{ margin: '20px', padding: '20px', background: '#f8f8f8', borderRadius: '10px' }}>
            <article
              id="post-1257"
              className="post-1257 post type-post status-publish format-standard has-post-thumbnail hentry category-current-exhibitions"
            >
              <div className="post_content_holder">
                <div className="post_text">
                  <div className="post_text_inner">
                    <p>&nbsp;</p>
                    <img
                      decoding="async"
                      src="../../Images/1.jpg"
                      alt="Paradboxes"
                      width="1060"
                      height="596"
                      style={{ marginBottom: '20px' }}
                    />
                    <p style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '10px' }}>Đậm ở Ném</p>
                    <p>
                      “In the workshop, you will tinker with Đậm and create a pen bag from existing belts, freely mix and match checkered
                      colors as you like, and you can also make a button keychain to hang on. bag. This will be a one-of-a-kind Pen roll
                      bag, one of a kind, but also made by your own hands, so proud.”
                    </p>
                    <p>
                      Instructions for knitting pen rolls and completing semi-finished products guided by Dai Lam: founder/designer of
                      @dambns
                    </p>
                    <p>The workshop is part of the launch series of Đậm&apos;s Free Perch collection</p>
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
      </div>
      <footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget" >
                        <h4>Ném Space</h4>
                        <p 
                        style={{fontWeight: '400', fontSize: '16px'}}
                        >
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Hours:</strong>
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}} >Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Blog01;
