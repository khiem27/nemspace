import React, { useRef, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SecurePayment() {
  const formRef = useRef(null);
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    cardNumber: '',
    cardHolder: '',
    expirationDate: '',
    cvc: '',
    zipCode: ''
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value
    }));
  };

  const handlePaymentSubmit = (event) => {
    event.preventDefault();

    // Simulate payment processing (replace this with actual payment API call)
    alert('Payment successful!');
  };

  return (
    <div className="payment-form-container" style={{ padding: '100px 20px 20px 20px' }}>
      <h2>Secure Payment</h2>
      <form onSubmit={handlePaymentSubmit} ref={formRef}>
        <div className="mb-3">
          <label htmlFor="cardNumber" className="form-label">
            Card Number:
          </label>
          <input
            type="text"
            className="form-control"
            id="cardNumber"
            name="cardNumber"
            value={formData.cardNumber}
            onChange={handleInputChange}
            placeholder="Enter card number"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="cardHolder" className="form-label">
            Card Holder:
          </label>
          <input
            type="text"
            className="form-control"
            id="cardHolder"
            name="cardHolder"
            value={formData.cardHolder}
            onChange={handleInputChange}
            placeholder="Enter card holder name"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="expirationDate" className="form-label">
            Expiration Date:
          </label>
          <input
            type="text"
            className="form-control"
            id="expirationDate"
            name="expirationDate"
            value={formData.expirationDate}
            onChange={handleInputChange}
            placeholder="MM/YYYY"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="cvc" className="form-label">
            CVC:
          </label>
          <input
            type="text"
            className="form-control"
            id="cvc"
            name="cvc"
            value={formData.cvc}
            onChange={handleInputChange}
            placeholder="Enter CVC"
          />
        </div>

        <div className="mb-3">
          <label htmlFor="zipCode" className="form-label">
            Zip Code:
          </label>
          <input
            type="text"
            className="form-control"
            id="zipCode"
            name="zipCode"
            value={formData.zipCode}
            onChange={handleInputChange}
            placeholder="Enter Zip Code"
          />
        </div>

        <button type="submit" className="btn btn-success" style={{ marginRight: '10px' }}>
          Pay Now
        </button>
        <button type="button" className="btn btn-danger" onClick={() => navigate(-1)}>
          Cancel
        </button>
      </form>

      
    </div>
  );
}

export default SecurePayment;
