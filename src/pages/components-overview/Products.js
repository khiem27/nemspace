import React from 'react';

Products.propTypes = {};

function Products() {
  const products = [
    {
      name: 'Bánh tráng Nhôm',
      price: '650.000đ',
      description: [
        'I had translated an aluminum plate into multi functional object couple months ago by passing by a cookware thrift shop and was attracted by these simple aluminum rounded shape plate.',
        'The plate was formed to different silhouette and explored different functions such as bookend, lamp shade, wall shelf, food tray and more to do.',
        'We developed the object with a different finishing technique that help aluminum harder, stronger and food safe itself. The Anodization.'
      ],
      details: {
        dimension: '15x15x30cm',
        material: 'food grade anodized aluminum',
        color: 'black',
        weight: '0.5kg',
        origin: 'Vietnam'
      },
      imageSrc: '../../Images/p1.jpg'
    },
    {
      name: 'The sexiest lamp',
      imageSrc: '../../Images/p2.jpg',
      description: [
        'Introducing Phễu lamp 03 by @nemproject',
        'A lamp made from reclaimed fan stand and PP and Acrylic shades.',
        'Using 4W 3000K LED bulb.',
        'Unique piece.'
      ],
      details: {
        material: 'Reclaimed Fan Stand',
        additionalInfo: 'Added PP and Acrylic shades',
        bulbDetails: 'Using 4W 3000K LED bulb',
        uniqueness: 'Unique piece',
        dimension: 'ø20cm x 40cm',
        weight: '0.5kg',
        origin: 'Vietnam',
        color: 'Black'
      },
      price: '2.800.000đ'
    },
    {
      imageSrc: '../../Images/p3.jpg',
      name: 'Time stops or keeps running',
      description: [
        'Introducing Phễu lamp 02 by @nemproject',
        'A lamp made from reclaimed fan stand and PP and Acrylic shades.',
        'Using 4W 3000K LED bulb.',
        'Unique piece.'
      ],
      details: {
        size: 'ø28cm',
        material: 'plywood and Mdf covered with acrylic',
        color: 'Black',
        weight: '0.5kg',
        origin: 'Vietnam'
      },
      price: '750,000 VND'
    },
    {
      imageSrc: '../../Images/p4.jpg',
      name: 'Borosilicate glass Carafe',
      description: ['Available in amber', 'Handmade in Vietnam', 'Dishwasher safe'],
      details: {
        dimension: '6.5x17.3cm 450ml',
        material: 'Borosilicate glass',
        color: 'Food grade color'
      },
      price: '420.000đ'
    },
    {
      imageSrc: '../../Images/p5.jpg',
      name: "Bang's candle-shaped lamp",
      description: [
        'Introducing Phễu lamp 01 by @nemproject',
        'A lamp made from reclaimed fan stand and PP and Acrylic shades.',
        'Using 4W 3000K LED bulb.',
        'Unique piece.'
      ],
      details: {
        bulbDetails: 'Use 7W 3000k bulb',
        prices: {
          powderCoatedIron: 650000,
          stainlessSteel: 670000
        }
      }
    },
    {
      imageSrc: '../../Images/p6.jpg',
      name: 'Modern style table lamp',
      description: [
        'Introducing Phễu lamp 01 by @nemproject',
        'A lamp made from reclaimed fan stand and PP and Acrylic shades.',
        'Using 4W 3000K LED bulb.',
        'Unique piece.'
      ],
      details: {
        dimensions: '15x18x50cm',
        lightDetails: '6W 220V, 2700k yellow light',
        material: 'Refinishing metal part'
      },
      price: '850,000 VND'
    },
    {
      imageSrc: '../../Images/p7.jpg',
      name: 'Wide: 22cm Height: 32cm',
      description: [
        'Introducing Phễu lamp 01 by @nemproject',
        'A lamp made from reclaimed fan stand and PP and Acrylic shades.',
        'Using 4W 3000K LED bulb.',
        'Unique piece.'
      ],
      details: {
        material: 'PP & Aluminum',
        bulbDetails: 'Using 7W LED bulb(recommended)',
        maxCapacity: 'Max capacity: 12W LED bulb',
        weight: '390g',
        edition: 'Third edition: 9'
      },
      price: '1.300.000VNĐ'
    }
  ];
  function renderNestedObject(obj) {
    return (
      <ul>
        {Object.entries(obj).map(([nestedKey, nestedValue]) => (
          <li key={nestedKey}>
            {nestedKey.charAt(0).toUpperCase() + nestedKey.slice(1)}: {nestedValue}
          </li>
        ))}
      </ul>
    );
  }

  return (
    <>
      {products.map((product, index) => {
        return (
          <div key={index} className="vc_row wpb_row section vc_row-fluid vc_inner" style={{ textAlign: 'left', paddingTop: '200px' }}>
            <div className="full_section_inner clearfix">
              <div className="storeelement wpb_column vc_column_container vc_col-sm-6">
                <div className="vc_column-inner vc_custom_1592181264932">
                  <div className="wpb_wrapper">
                    <div className="wpb_gallery wpb_content_element vc_clearfix">
                      <div className="wpb_wrapper">
                        <div
                          className="wpb_gallery_slides has_control_nav wpb_flexslider flexslider_slide flexslider"
                          data-interval="3"
                          data-flex_fx="slide"
                          data-control="true"
                          data-direction="true"
                          data-pause-on-hover="false"
                        >
                          <div className="flex-viewport" style={{ overflow: 'hidden', position: 'relative' }}>
                            <ul
                              className="slides"
                              style={{
                                display: 'flex',
                                justifyContent: 'center'
                              }}
                            >
                              <li className="clone" aria-hidden="true" style={{ width: '583.5px', display: 'block' }}>
                                <a
                                  className="qode-prettyphoto"
                                  data-rel="prettyPhoto[rel-654-2893478892]"
                                  rel="prettyPhoto[rel-654-2893478892]"
                                >
                                  <img
                                    loading="lazy"
                                    decoding="async"
                                    width="1000"
                                    height="1000"
                                    src={product.imageSrc}
                                    className="attachment-full"
                                    alt=""
                                    sizes="(max-width: 1000px) 100vw, 1000px"
                                    draggable="false"
                                  />
                                </a>
                              </li>
                            </ul>
                          </div>
                          <ul className="flex-direction-nav">
                            <li className="flex-nav-prev">
                              <a className="flex-prev" href="#">
                                <div>
                                  <i className="fa fa-angle-left" aria-hidden="true"></i>
                                </div>
                              </a>
                            </li>
                            <li className="flex-nav-next">
                              <a className="flex-next" href="#">
                                <div>
                                  <i className="fa fa-angle-right" aria-hidden="true"></i>
                                </div>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div className="vc_empty_space" style={{ height: '32px' }}>
                      <span className="vc_empty_space_inner">
                        <span className="empty_space_image"></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="wpb_column vc_column_container vc_col-sm-6">
                <div className="vc_column-inner vc_custom_1592181518019">
                  <div className="wpb_wrapper">
                    <div className="wpb_text_column wpb_content_element">
                      <div className="wpb_wrapper">
                        <h2>{product.name}</h2>
                      </div>
                    </div>

                    <h4 className="clearfix qode-title-holder ui-accordion-header ui-corner-top">
                      <span className="qode-tab-title">
                        <h3
                          className="qode-tab-title-inner"
                          style={{
                            color: 'orange'
                          }}
                        >
                          Price: {product.price}{' '}
                        </h3>
                        <button
                          type="button"
                          className="qbutton left default"
                          onClick={(e) => {
                            e.preventDefault();
                          }}
                        >
                          Add to Cart ️🛒
                        </button>
                      </span>
                    </h4>

                    <div className="wpb_text_column wpb_content_element">
                      <div className="wpb_wrapper">
                        {product.description && product.description.map((paragraph, index) => <h5 key={index}>{paragraph}</h5>)}
                      </div>
                    </div>

                    <div className="qode-accordion-holder clearfix qode-toggle qode-initial accordion ui-accordion ui-accordion-icons ui-widget ui-helper-reset">
                      <h4 className="clearfix qode-title-holder ui-accordion-header ui-corner-top ui-accordion-header-active ui-state-active">
                        <span className="qode-tab-title">
                          <span className="qode-tab-title-inner"> ⬜ Product Details </span>
                        </span>
                        <span className="qode-accordion-mark">
                          <span className="qode-accordion-mark-icon">
                            <span className="icon_plus"></span>
                            <span className="icon_minus-06"></span>
                          </span>
                        </span>
                      </h4>
                      <div className="qode-accordion-content qode-acc-title-with-icon ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">
                        <div className="qode-accordion-content-inner">
                          <div className="wpb_text_column wpb_content_element">
                            <div className="wpb_wrapper">
                              {product.details && (
                                <ul>
                                  {Object.entries(product.details).map(([key, value]) => (
                                    <li key={key}>
                                      {key.charAt(0).toUpperCase() + key.slice(1)}:{' '}
                                      {typeof value === 'object' ? renderNestedObject(value) : value}
                                    </li>
                                  ))}
                                </ul>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}

<footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget" >
                        <h4>Ném Space</h4>
                        <p 
                        style={{fontWeight: '400', fontSize: '16px'}}
                        >
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>
                          <strong>Hours:</strong>
                        </p>
                        <p style={{fontWeight: '400', fontSize: '16px'}} >Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p style={{fontWeight: '400', fontSize: '16px'}}>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Products;
