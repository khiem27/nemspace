import React from 'react';

AboutUs.propTypes = {};

function AboutUs() {
  return (
    <div className="content content_top_margin" style={{ minHeight: '86px' }}>
      <div className="content_inner">
        <div className="full_width" style={{ backgroundColor: '#ffffff' }}>
          <div className="full_width_inner">
            <div
              className="vc_row wpb_row section vc_row-fluid grid_section"
              style={{ paddingTop: '30px', paddingBottom: '65px', textAlign: 'center' }}
            >
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner vc_custom_1589383271967">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h1>Who We Are</h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="vc_row wpb_row section vc_row-fluid grid_section" style={{ paddingBottom: '95px', textAlign: 'center' }}>
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner vc_custom_1589383271967">
                      <div className="wpb_wrapper">
                        <div className="wpb_single_image wpb_content_element vc_align_left">
                          <div className="wpb_wrapper">
                            <div className="vc_single_image-wrapper vc_box_border_grey">
                              <img
                                decoding="async"
                                width="1366"
                                height="993"
                                src="../../Images/5.jpg"
                                className="vc_single_image-img attachment-full"
                                alt=""
                                sizes="(max-width: 1060px) 100vw, 1060px"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="vc_row wpb_row section vc_row-fluid about_text grid_section"
              style={{ paddingBottom: '60px', textAlign: 'left' }}
            >
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-3">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h3 style={{ textAlign: 'right' }}>VISION</h3>
                          </div>
                        </div>
                        <div className="vc_empty_space" style={{ height: '32px' }}>
                          <span className="vc_empty_space_inner">
                            <span className="empty_space_image"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="wpb_column vc_column_container vc_col-sm-9">
                    <div className="vc_column-inner vc_custom_1589383271967">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h5>
                              Ném Space strives to be viewed as a credible and change people&apos;s thinking about items that are considered
                              useless; while empowering artists with the facilities, environment and experience to excel in their medium and
                              enrich their lives.
                            </h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              className="vc_row wpb_row section vc_row-fluid about_text grid_section"
              style={{ paddingBottom: '60px', textAlign: 'left' }}
            >
              <div className="section_inner clearfix">
                <div className="section_inner_margin clearfix">
                  <div className="wpb_column vc_column_container vc_col-sm-3">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h3 style={{ textAlign: 'right' }}>MISSION</h3>
                          </div>
                        </div>
                        <div className="vc_empty_space" style={{ height: '32px' }}>
                          <span className="vc_empty_space_inner">
                            <span className="empty_space_image"></span>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="wpb_column vc_column_container vc_col-sm-9">
                    <div className="vc_column-inner vc_custom_1589383271967">
                      <div className="wpb_wrapper">
                        <div className="wpb_text_column wpb_content_element">
                          <div className="wpb_wrapper">
                            <h5>New enjoyable environment while environmentally friendly</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget">
                        <h4>Ném Space</h4>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>
                          <strong>Hours:</strong>
                        </p>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p style={{ fontWeight: '400', fontSize: '16px' }}>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default AboutUs;
