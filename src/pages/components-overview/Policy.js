import React from 'react';

const Policy = () => {
  return (
    <><div style={{ maxWidth: '800px', margin: '0 auto', padding: '100px 20px 20px 20px' }}>
      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>Who we are</h2>
      <p>
        Our website address is: <a href="https://nemspace.vercel.app/">https://nemspace.vercel.app/</a>
      </p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>Media</h2>
      <p>
        If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to
        the website can download and extract any location data from images on the website.
      </p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>Cookies</h2>
      <p>
        If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your
        convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one
        year.
      </p>
      <p>
        If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no
        personal data and is discarded when you close your browser.
      </p>
      <p>
        When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies
        last for two days, and screen options cookies last for a year. If you select “Remember Me”, your login will persist for two weeks.
        If you log out of your account, the login cookies will be removed.
      </p>
      <p>
        If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and
        simply indicates the post ID of the article you just edited. It expires after 1 day.
      </p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>Who we share your data with</h2>
      <p>If you request a password reset, your IP address will be included in the reset email.</p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>How long we retain your data</h2>
      <p>
        If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any
        follow-up comments automatically instead of holding them in a moderation queue.
      </p>
      <p>
        For users that register on our website (if any), we also store the personal information they provide in their user profile. All
        users can see, edit, or delete their personal information at any time (except they cannot change their username). Website
        administrators can also see and edit that information.
      </p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>What rights you have over your data</h2>
      <p>
        If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold
        about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This
        does not include any data we are obliged to keep for administrative, legal, or security purposes.{' '}
      </p>

      <h2 style={{ color: '#333', fontSize: '24px', marginBottom: '10px' }}>Where your data is sent</h2>
      <p>Visitor comments may be checked through an automated spam detection service.</p>

    </div><footer>
        <div className="footer_inner clearfix">
          <div className="footer_top_holder">
            <div className="footer_top footer_top_full">
              <div className="three_columns clearfix">
                <div className="column1 footer_col1">
                  <div className="column_inner">
                    <div id="text-6" className="widget widget_text">
                      <h5>ABOUT US</h5>
                      <div className="textwidget">
                        <h4>Ném Space</h4>
                        <p>
                          <strong>Phone:</strong> 0913119034
                        </p>
                        <p>
                          <strong>Email:</strong> chao.nemproject@gmail.com
                        </p>
                        <p>
                          <strong>Address:</strong> 18/1 Ngô Thời Nhiệm, phường Võ Thị Sáu, Quận 3, Hồ Chí Minh, Việt Nam
                        </p>
                        <p>
                          <strong>Hours:</strong>
                        </p>
                        <p>Monday - Friday : 9 am - 22 pm ( Coffee and Cocktail Bar)</p>
                        <p>Saturday - Sunday: 11:00 am – 4:00 pm ( Workshop )</p>
                        <p>&nbsp;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column2 footer_col2">
                  <div className="column_inner">
                    <div id="text-5" className="widget widget_text">
                      <h5>MADE POSSIBLE BY</h5>
                      <div className="textwidget">
                        <div className="wpb_single_image wpb_content_element vc_align_center">
                          <div className="wpb_wrapper">
                            <a href="https://www.visitutah.com" target="_blank" rel="noopener noreferrer">
                              <div className="vc_single_image-wrapper vc_box_border_grey">
                                <p>This website is for academic purposes only, not for business purposes.</p>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="column3 footer_col3">
                  <div className="column_inner">
                    <div id="text-2" className="widget widget_text">
                      <h5>JOIN THE CONVERSATION</h5>
                      <div className="textwidget"></div>
                    </div>
                    <div id="custom_html-2" className="widget_text widget widget_custom_html">
                      <div className="textwidget custom-html-widget">
                        {/* ig icon */}
                        <div className="ig-icon">
                          <a href="#" target="_blank" rel="noopener noreferrer">
                            <img
                              src="../../Images/instagram.png"
                              alt="instagram"
                              style={{ width: '50px', height: '50px', marginRight: '10px' }} />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer_bottom_holder">
          <div className="three_columns footer_bottom_columns clearfix">
            <div className="column1 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="textwidget">© 2023 OGDEN CONTEMPORARY ARTS. All Rights Reserved.</div>
                </div>
              </div>
            </div>
            <div className="column2 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom"></div>
              </div>
            </div>
            <div className="column3 footer_bottom_column">
              <div className="column_inner">
                <div className="footer_bottom">
                  <div className="menu-footer-bottom-container">
                    <ul
                      id="menu-footer-bottom"
                      className="menu"
                      style={{
                        display: 'flex',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                        gap: '20px'
                      }}
                    >
                      <li
                        id="menu-item-37279"
                        className="menu-item menu-item-type-post_type menu-item-object-page menu-item-privacy-policy menu-item-37279"
                      >
                        <a rel="privacy-policy" href="https://ogdencontemporaryarts.org/privacy-policy/">
                          Privacy Policy
                        </a>
                      </li>
                      <li id="menu-item-37280" className="menu-item menu-item-type-post_type menu-item-object-page menu-item-37280">
                        <a href="https://ogdencontemporaryarts.org/return-policy/">Return Policy</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer></>
  );
};

export default Policy;
